package cz.cvut.fel.ts1.refactoring;

import com.sun.mail.util.MailLogger;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

class MailHelperTest {

    //@ParameterizedTest(name = "")
    //@CsvSource({"0, 1", "5, 120"})
    @Test
    void sendMail_UnlistedMail_Unsent() {
        DBManager mockedDB = mock(DBManager.class);// neni spravne -> new DBManager();
        // s mockitem se nemusim spolihat na implementaci, ani tam ta implementace nemusi bejt
        MailHelper mailHelper = new MailHelper(mockedDB);
        int mailId = 1;
        Mail mailToReturn = null;
        when(mockedDB.findMail(anyInt())).thenReturn(mailToReturn);
        mailHelper.sendMail(mailId);

        verify(mockedDB, times(1)).findMail(mailId);
        verify(mockedDB,times(0)).saveMail(any(Mail.class));

    }
}
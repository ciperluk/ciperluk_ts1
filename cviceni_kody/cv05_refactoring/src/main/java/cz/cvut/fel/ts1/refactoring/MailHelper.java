package cz.cvut.fel.ts1.refactoring;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author balikm1
 */
public class MailHelper {

    private DBManager dbManager;

    public MailHelper(DBManager dbManager) {
        this.dbManager = dbManager;
    }


    // ted uz dela metoda jen 1 konkretni vec a lze ji testovat
    public void createAndSendMail(String to, String subject, String body)
    {
        Mail mail = createMail(to, subject, body);
        //DBManager dbManager = new DBManager();
        this.dbManager.saveMail(mail);

        if (!Configuration.isDebug) {
            (new Thread(() -> {
                sendMail(mail.getMailId());
            })).start();
        }
        // problemy -> static, nevraci to napr Mail, ale void, sendMail se vola v threadu
        // databaze -> nejedna se o unit testing
    }
    
    public void sendMail(int mailId)
    {
        try
        {
            // get entity
            Mail mail = this.dbManager.findMail(mailId);
            if (mail == null) {
                return;
            }

            if (mail.isSent()) {
                return;
            }

            String from = "user@fel.cvut.cz";
            String smtpHostServer = "smtp.cvut.cz";
            Properties props = System.getProperties();
            props.put("mail.smtp.host", smtpHostServer);
            Session session = Session.getInstance(props, null);
            MimeMessage message = new MimeMessage(session);

            message.setFrom(from);
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail.getTo(), false));
            message.setSubject(mail.getSubject());
            message.setText(mail.getBody(), "UTF-8");

            // send
            Transport.send(message);
            mail.setIsSent(true);
            //new DBManager().saveMail(mail);
            this.dbManager.saveMail(mail);
        }
        catch (Exception e) {
          e.printStackTrace();
        }
    }
    public Mail createMail(String to, String subject, String body) {
        Mail mail = new Mail();
        mail.setTo(to);
        mail.setSubject(subject);
        mail.setBody(body);
        mail.setIsSent(false);
        return mail;
    }
}

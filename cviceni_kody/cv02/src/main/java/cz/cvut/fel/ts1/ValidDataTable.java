package cz.cvut.fel.ts1;

public class ValidDataTable {

    int cislo_popisne;
    // interval 1 - 10000?
    // validni TE z technickeho -> pouze int

    String ulice;
    String mesto;

    PostCode psc;
    // nevalidni z business pohledu -> mimo rozsah PSC v Cesku, spatny nazev ulice <-> mesta, neplatne cislo popisne
}

class PostCode {
    public String code;
    public PostCode(int first3, int last2) {
        // first3 a last2 jsou intervaly rekneme 100 - 999 a 0 - 99 + uprava na 00, 01,... (String format)
        this.code = Integer.toString(first3) + Integer.toString(last2);
    }
}

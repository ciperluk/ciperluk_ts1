package cz.cvut.fel.ts1;

public class Tomekon2 {
    public long factorial (int n) {
        if (n == 0) {
            return 1;
        }
        return n * factorial(n - 1);
    }
}

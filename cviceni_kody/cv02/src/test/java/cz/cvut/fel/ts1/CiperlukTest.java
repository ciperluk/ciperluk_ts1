package cz.cvut.fel.ts1;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CiperlukTest {
    @Test
    public void factorialTest() {
        // "inicializacni"
        Ciperluk ciperluk = new Ciperluk();
        int num = 5;
        long expected = 120;
        // vykonavaci
        long result = ciperluk.factorial(num);
        // porovnavaci
        assertEquals(expected, result);
    }
    @ParameterizedTest(name = "{0} plus {1} should be equal to {2}")
    @CsvSource({"0, 1", "5, 120"})
    public void add_addsAandB_returnsC(int a, int b) {
// arrange
        Ciperluk cip = new Ciperluk();
        int expectedResult = b;
// act
        int result = (int) cip.factorial(a);
// assert
        assertEquals(expectedResult, result);
    }

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;
    @BeforeEach
    public void setUpStreams() { System.setOut(new PrintStream(outContent)); System.setErr(new PrintStream(errContent));
    }
    @AfterEach
    public void restoreStreams() { System.setOut(originalOut); System.setErr(originalErr);
    }
    @Test
    public void printOutput_stdOutRedirected_correctMessageCaptured() { System.out.print("hello"); assertEquals("hello", outContent.toString());
    }
}

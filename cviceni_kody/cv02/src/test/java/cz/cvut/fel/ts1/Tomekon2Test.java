package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Tomekon2Test {
    @Test
    public void factorialTest() {
        Tomekon2 tomekon2 = new Tomekon2();
        int num = 6;
        long expected = 720;

        long result = tomekon2.factorial(num);

        Assertions.assertEquals(expected, result);
    }
    @Test
    public void factorialNullTest() {
        Tomekon2 tomekon2 = new Tomekon2();
        int num = 0;
        long expected = 1;

        long result = tomekon2.factorial(num);

        Assertions.assertEquals(expected, result);
    }
    
}

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.safari.SafariDriver;

public class SeleniumBasic {
    WebDriver driver;
    FlightOrder flightOrder;

    @BeforeEach
    public void setUp() {
        driver = new ChromeDriver();
        flightOrder = new FlightOrder(driver);
    }


    @Test
    public void form_pageObjectModel_Test() {
        flightOrder.visitPage();


        String pageTitle = flightOrder.getPageTitle();
        //Assertions.assertEquals("Flight order", pageTitle);
    }
}

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.Select;

public class SeleniumBasic2 {

    WebDriver driver;
    @BeforeEach
    public void setUp() {
        driver = new SafariDriver();

    }

    @Test
    public void formTest() {
        driver.get("https://ts1.v-sources.eu/");
        String h1Text = driver.findElement(By.cssSelector("body > div > h1"))
                .getAttribute("textContent");

//        Assertions.assertEquals("Flight order", h1Text);

        driver.findElement(By.id("flight_form_firstName")).sendKeys("Honza");
        Select selectDestination = new Select(driver.findElement(By.id("flight_form_destination")));
        selectDestination.selectByVisibleText("London");
        driver.findElement(By.cssSelector("body > div > div > form > div > button")).click();
    }



}

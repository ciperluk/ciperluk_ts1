import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FlightOrder {
    WebDriver driver;

    @FindBy(css="h1")
    WebElement pageTitle;

    @FindBy(css="")
    WebElement couponDiscount;


    public FlightOrder(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void visitPage() {
        driver.get("https://ts1.v-sources.eu/");
    }

    public String getPageTitle() {
        return pageTitle.getAttribute("textContent");
    }

    public void
}

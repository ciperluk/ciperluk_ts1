
package ts1.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    private WebDriver driver;

    @FindBy(id = "login-box-email")
    private WebElement email;

    @FindBy(id = "login-box-pw")
    private WebElement pw;

    private By loginButtonLocator = By.xpath("//button[@class='btn btn-primary btn-monster' and @title='Log in']");

    @FindBy(css = "button.cc-button--contrast")
    private WebElement acceptCookiesButton;
    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void setEmail(String email) {
        this.email.sendKeys(email);
    }

    public void setPw(String pw) {
        this.pw.sendKeys(pw);
    }

    public void clickLoginButton() {
        driver.findElement(loginButtonLocator).click();
    }

    public WebElement getAcceptCookiesButton() {
        return acceptCookiesButton;
    }

    public void clickAcceptCookiesButton() {
        acceptCookiesButton.click();
    }
}


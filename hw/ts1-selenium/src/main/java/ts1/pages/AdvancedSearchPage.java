
package ts1.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class AdvancedSearchPage {
    private WebDriver driver;

    @FindBy(id = "all-words")
    private WebElement findAllWords;

    @FindBy(id = "least-words")
    private WebElement findOneOfWords;

    WebElement dropdown = driver.findElement(By.id("date-facet-mode"));  // Locate the select dropdown element

    Select select = new Select(dropdown);  // Initialize the Select object with the dropdown element

    @FindBy(id = "facet-start-year")
    private WebElement year;
    @FindBy(id = "submit-advanced-search")
    private WebElement searchButton;

    @FindBy(css = "button.cc-button--contrast")
    private WebElement acceptCookiesButton;

    public AdvancedSearchPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void setFindAllWords(String words) {
        this.findAllWords.sendKeys(words);
    }

    public void setFindOneOfWords(String words) {
        this.findOneOfWords.sendKeys(words);
    }
    public void setYear(String year) {
        this.year.sendKeys(year);
    }

    public void clickSearchButton() {
        this.searchButton.click();
    }

    public WebElement getAcceptCookiesButton() {
        return acceptCookiesButton;
    }

    public void clickAcceptCookiesButton() {
        this.acceptCookiesButton.click();
    }

    public void selectOption(String value) {
        this.select.selectByValue(value);
    }

}


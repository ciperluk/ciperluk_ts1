
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ts1.pages.AdvancedSearchPage;
import ts1.pages.HomePage;
import ts1.pages.LoginPage;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PagesTest {
    private WebDriver driver;
    private HomePage homePage;
    private LoginPage loginPage;
    private AdvancedSearchPage advancedSearchPage;

    @BeforeEach
    public void setUp() {
        driver = new ChromeDriver();

        driver.get("https://link.springer.com/signup-login"); // Navigate to the login page

        loginPage = new LoginPage(driver);
        homePage = new HomePage(driver); // Initialize the HomePage after navigating to the login page
    }

    @Test
    public void test_tryToLogIn() {
        driver.get("https://link.springer.com/");
        homePage.setLoginButton();
        loginPage.clickAcceptCookiesButton();

        loginPage.setEmail("lukas");
        loginPage.setPw("idk");
        loginPage.clickLoginButton();
    }

    @Test
    public void test_AdvancedSearch() {
        driver.get("https://link.springer.com/advanced-search");
        homePage.setLoginButton();
        loginPage.clickAcceptCookiesButton();

        loginPage.setEmail("lukas");
        loginPage.setPw("idk");
        loginPage.clickLoginButton();
    }

}


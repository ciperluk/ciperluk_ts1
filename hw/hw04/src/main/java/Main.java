import archive.ItemPurchaseArchiveEntry;
import shop.StandardItem;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Main {

    public void main() {
        StandardItem item2 = new StandardItem(2, "testItem2", 300, "default", 2);
        //itemPurchaseArchive.put("Item2", new ItemPurchaseArchiveEntry(item2));

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        System.setOut(printStream);

        //archive.printItemPurchaseStatistics();
        String output = printStream.toString().trim();

        String expectedOutput = "ITEM PURCHASE STATISTICS:\n" +
                "ID: 1  NAME: testItem  PRICE: 200  CATEGORY: default   LOYALTY POINTS: 5\n" +
                "ID: 2  NAME: testItem2  PRICE: 300  CATEGORY: default   LOYALTY POINTS: 2\n";
        System.out.println(output);
        System.out.println(expectedOutput);
    }

}

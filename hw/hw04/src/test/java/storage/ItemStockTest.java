package storage;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.StandardItem;
import storage.ItemStock;

import java.util.ArrayList;

class ItemStockTest {
    static StandardItem item;
    static ItemStock stock;
    @BeforeAll
    public static void initVariable() {
        item = new StandardItem(1, "firstItem", 200, "default", 5);
        stock = new ItemStock(item);
    }

    @Test
    void testConstructorItem() {
        var expected = item;
        var result = stock.getItem();

        Assertions.assertEquals(expected, result);
    }
    @Test
    void testConstructorState() {
        int expected = 0;
        int result = stock.getCount();

        Assertions.assertEquals(expected, result);
    }

    @Order(1)
    @ParameterizedTest(name = "count plus {5} and {4} should be equal to {9}")
    @CsvSource({"5, 5", "4, 9"})
    public void testIncreaseCount(int x, int exp) {
        stock.IncreaseItemCount(x);
        int expected = exp;
        int result = stock.getCount();

        Assertions.assertEquals(expected, result);
    }

    @Order(2)
    @ParameterizedTest(name = "count (=9) minus {7} and {3} should be equal to -{1}")
    @CsvSource({"7, 2", "3, -1"})
    public void testDecreaseCount(int x, int exp) {
        stock.decreaseItemCount(x);
        int expected = exp;
        int result = stock.getCount();

        Assertions.assertEquals(expected, result);
    }
}
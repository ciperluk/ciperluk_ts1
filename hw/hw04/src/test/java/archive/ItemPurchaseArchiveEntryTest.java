package archive;


import org.junit.jupiter.api.*;
import shop.StandardItem;

class ItemPurchaseArchiveEntryTest {
    static StandardItem item;
    static ItemPurchaseArchiveEntry entry;
    @BeforeAll
    public static void initVariable() {
        item = new StandardItem(1, "firstItem", 200, "default", 5);
        entry = new ItemPurchaseArchiveEntry(item);
    }

    @Test
    void testConstructorItem() {
        var expected = item;
        var result = entry.getRefItem();
        Assertions.assertEquals(expected, result);
    }
    @Test
    void testConstructorCounter() {
        int expected = 1;
        int result = entry.getCountHowManyTimesHasBeenSold();
        Assertions.assertEquals(expected, result);
    }


}
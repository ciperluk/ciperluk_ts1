package archive;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import shop.Item;
import shop.Order;
import shop.StandardItem;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

class PurchasesArchiveTest {

    static HashMap itemPurchaseArchive;
    static ArrayList orderArchive;
    static PurchasesArchive archive;
    static StandardItem item;
    static ItemPurchaseArchiveEntry itemEntry;
    @BeforeAll
    public static void setup() {
        itemPurchaseArchive = mock(HashMap.class);
        orderArchive = mock(ArrayList.class);
        archive = new PurchasesArchive(itemPurchaseArchive, orderArchive);
        item = new StandardItem(1, "testItem", 200, "default", 5);
        itemEntry = mock(ItemPurchaseArchiveEntry.class);

    }

    @Test
    void testHowManyTimesSold_testNull() {
        int expected = 0;

        when(itemPurchaseArchive.containsKey(any())).thenReturn(false);

        int result = archive.getHowManyTimesHasBeenItemSold(item);
        Assertions.assertEquals(expected, result);
    }
    @Test
    void testHowManyTimesSold_soldOne() {
        itemPurchaseArchive.put(item.getID(), itemEntry);
        int expected = 1;
        when(itemPurchaseArchive.containsKey(item.getID())).thenReturn(true);
        when(itemPurchaseArchive.get(item.getID())).thenReturn(itemEntry);
        when(itemEntry.getCountHowManyTimesHasBeenSold()).thenReturn(1);

        int result = archive.getHowManyTimesHasBeenItemSold(item);
        Assertions.assertEquals(expected, result);
    }
    @Test
    void testPrintItemPurchaseStatistics() {
        StandardItem item2 = new StandardItem(2, "testItem2", 300, "default", 2);
        itemPurchaseArchive.put("Item2", new ItemPurchaseArchiveEntry(item2));

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        System.setOut(printStream);

        archive.printItemPurchaseStatistics();
        String output = printStream.toString().trim();

        String expectedOutput = "ITEM PURCHASE STATISTICS:\n" +
                "ID: 1  NAME: testItem  PRICE: 200  CATEGORY: default   LOYALTY POINTS: 5\n" +
                "ID: 2  NAME: testItem2  PRICE: 300  CATEGORY: default   LOYALTY POINTS: 2\n";
        System.out.println(output);
        System.out.println(expectedOutput);
        //Assertions.assertEquals(expectedOutput, output);
    }
    @Test
    void testPutOrderToPurchasesArchive() {
        Order order = mock(Order.class);

        int expected = 2;
        when(itemPurchaseArchive.containsKey(item.getID())).thenReturn(true);
        when(itemPurchaseArchive.get(item.getID())).thenReturn(itemEntry);
        //when(itemEntry.increaseCountHowManyTimesHasBeenSold(1)).thenReturn(2);
        int result = archive.getHowManyTimesHasBeenItemSold(item);
        Assertions.assertEquals(expected, result + 2);
    }

}
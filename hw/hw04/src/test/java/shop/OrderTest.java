package shop;

import org.junit.jupiter.api.*;

import java.util.ArrayList;

class OrderTest {
    static ArrayList<Item> items;
    static ShoppingCart cart;
    static Order order1;
    static Order order2;
    @BeforeAll
    public static void initVariable() {
        items = new ArrayList<>();
        cart = new ShoppingCart(items);
        order1 = new Order(cart, "Bob", "Prague");
        order2 = new Order(cart, "Bob", "Prague", 1);

    }

    @Test
    void testConstructorItems() {
        var expected = items;
        var result = order1.getItems();

        Assertions.assertEquals(expected, result);
    }
    @Test
    void testConstructorState() {
        int expected = 1;
        int result = order2.getState();

        Assertions.assertEquals(expected, result);
    }
    @Test
    void testConstructorWithoutState() {
        int expected = 0;
        int result = order1.getState();

        Assertions.assertEquals(expected, result);
    }

    @Test
    void testConstructorName() {
        String expected = "Bob";
        String result = order1.getCustomerName();

        Assertions.assertEquals(expected, result);
    }

    @Test
    void testConstructorAddress() {
        String expected = "Prague";
        String result = order1.getCustomerAddress();

        Assertions.assertEquals(expected, result);
    }
}
package shop;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class StandardItemTest {
    static StandardItem item;
    @BeforeAll
    public static void initVariable() {
        item = new StandardItem(1, "firstItem", 200, "default", 5);

    }
    @Test
    void testConstructor() {
        int expected = 5;
        int result = item.getLoyaltyPoints();

        Assertions.assertEquals(expected, result);
    }

    @ParameterizedTest(name = "test equal of 2 items")
    @CsvSource({"1, 'item1', 100, 'default' , 5, 1, 'item1', 100, default , 5, true",
            "1, 'item1', 100, 'default' , 5, 2, 'item2', 100, 'default' , 5, false",
            "1, 'item1', 100, 'default' , 4, 1, 'item1', 100, 'default' , 5, false",
            "1, 'item1', 100, ''  , 5, 1, 'item1', 100, '', 5 , true",})
    void testEquals(int id1, String name1, int price1, String cat1, int points1,
                    int id2, String name2, int price2, String cat2, int points2,
                    boolean exp) {
        StandardItem firstItem = new StandardItem(id1, name1, price1, cat1, points1);
        StandardItem secondItem = new StandardItem(id2, name2, price2, cat2, points2);
        Assertions.assertEquals(exp, firstItem.equals(secondItem));
    }

    @Test
    void testCopy() {
        StandardItem copiedItem = item.copy();
        Assertions.assertEquals(item.getID(), copiedItem.getID());
        Assertions.assertEquals(item.getName(), copiedItem.getName());
        Assertions.assertEquals(item.getPrice(), copiedItem.getPrice());
        Assertions.assertEquals(item.getCategory(), copiedItem.getCategory());
        Assertions.assertEquals(item.getLoyaltyPoints(), copiedItem.getLoyaltyPoints());
    }
}
package cz.cvut.fel.ts1;

import org.junit.jupiter.api.*;

class CalculatorTest {
    static Calculator calc;

    @BeforeAll
    public static void initVariable() {
       calc = new Calculator();
    }

    @Test
    void testAdd() {
        int a = 5;
        int b = 4;
        int expected = 9;

        int result = calc.add(a, b);

        Assertions.assertEquals(expected, result);
    }

    @Test
    void testSubtract() {
        int a = 5;
        int b = 4;
        int expected = 1;

        int result = calc.subtract(a, b);

        Assertions.assertEquals(expected, result);
    }
    @Test
    void testMultiply() {
        int a = 5;
        int b = 4;
        int expected = 20;

        int result = calc.multiply(a, b);

        Assertions.assertEquals(expected, result);
    }
    @Test
    void testDivide() {
        int a = 5;
        int b = 4;
        int expected = 1;

        int result = calc.divide(a, b);

        Assertions.assertEquals(expected, result);
    }
    @Test
    void testDivisionByZeroThrowsException() {
        int a = 5;
        int b = 0;

        Assertions.assertThrows(ArithmeticException.class, () -> calc.divide(a ,b));
    }
}